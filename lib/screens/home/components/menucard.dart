import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
class MenuCard extends StatelessWidget {
  const MenuCard({
    Key key,
    @required this.icon,
    @required this.text,
    @required this.press,
  }) : super(key: key);

  final String icon, text;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: SizedBox(
        width: 95,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              height: 40,
              width: 95,
              decoration: BoxDecoration(
                color: Color(0xFFFFFFFF),
                borderRadius: BorderRadius.circular(0),
              ),
              child: SvgPicture.asset(icon),
            ),
            SizedBox(height: 5),
            Text(text, textAlign: TextAlign.center,style: TextStyle(fontSize: 16, color: Colors.black54,fontWeight:FontWeight.w400 ),)
          ],
        ),
      ),
    );
  }
}
