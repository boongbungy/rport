import 'package:flutter/material.dart';
import 'package:rportfarmer/common/constants.dart';
import 'package:rportfarmer/common/home_header.dart';
import 'package:rportfarmer/screens/home/components//banners.dart';
import 'package:rportfarmer/screens/home/components/hotmenu.dart';

import 'package:rportfarmer/screens/login/login_screen.dart';

import 'background_image.dart';
import 'maintext.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          BackgroundImage(),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //MainText(),
              Container(
                height: size.height * 0.5,
                color: white,
                child: Column(
                  children: [
                    HotMenu(),
                    //Banners(texttop: "Banner\n",textbottom: "Promotion",color: Color(0xFF4A3298)),

                  ],
                ),
              ),
            ],
          ),
          //HomeHeader(),
          //Banners(texttop: "จำนวนเงิน\n",textbottom: "2,000",color: kPrimaryColor,),
        ],
      ),
    );
  }
}
