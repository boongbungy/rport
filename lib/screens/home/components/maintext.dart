import 'package:flutter/material.dart';
import 'package:rportfarmer/common/constants.dart';


class MainText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('จำนวนเงิน',style: TextStyle(
            color: yellow,
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),),
          SizedBox(height: 5,),
          Text('2,000',style: TextStyle(
            color: white,
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),),

        ],
      ),
    );
  }
}
