import 'package:flutter/material.dart';
import 'package:rportfarmer/size_config.dart';
class Banners extends StatelessWidget {
  const Banners({
    Key key,
    this.texttop,
    this.textbottom,
    this.color,
  }) : super(key: key);
  final String texttop;
  final String textbottom;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.all(20.0),
        padding: EdgeInsets.symmetric(
          horizontal: 20.0,
          vertical: 15.0,
        ),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(20),
        ),child: Text.rich(
      TextSpan(
        style: TextStyle(color: Colors.white),
        children: [
          TextSpan(text: texttop),
          TextSpan(
            text: textbottom,
            style: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    ),
    );
  }
}
