import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rportfarmer/common/constants.dart';
import 'package:rportfarmer/screens/form/receipt.dart';
import 'package:rportfarmer/screens/home/components/menucard.dart';
class Menus extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List.generate(
          menus.length,
              (index) => MenuCard(
            icon: menus[index]["icon"],
            text: menus[index]["text"],
            press: () {
              MaterialPageRoute materialPageRoute =
              MaterialPageRoute(builder: (BuildContext context) =>Receipt(menuID:index));
              Navigator.of(context).push(materialPageRoute);
            },
          ),
        ),
      ),
    );
  }
}

