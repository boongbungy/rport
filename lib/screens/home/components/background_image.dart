import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rportfarmer/common/constants.dart';
import 'package:rportfarmer/screens/home/components/maintext.dart';
import 'package:rportfarmer/viewmodels/home_view_model.dart';

class BackgroundImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;
    return ViewModelProvider<HomeViewModel>.withConsumer(
      viewModelBuilder: () => HomeViewModel(),
      onModelReady: (model) => model.handleData(),
      builder: (context, model, child) => Stack(
        alignment: Alignment.bottomLeft,
        children: [
          Image(
            height: size.height * 0.41,
            fit: BoxFit.cover,
            image: AssetImage('assets/images/rubber.jpg'),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('จำนวนเงิน',style: TextStyle(
                  color: yellow,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),),
                SizedBox(height: 5,),
                Text(model.amount.toString().replaceAllMapped(new RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'), (Match m) => '${m[1]},'),style: TextStyle(
                  color: white,
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),),

              ],
            ),
          ),
        ],
      ),
    );


  }
}