import 'package:flutter/material.dart';
import 'package:rportfarmer/screens/home/components//banners.dart';
import 'package:rportfarmer/screens/home/components/menus.dart';
class DashBoardView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child:SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Banners(texttop: "Banner\n",textbottom: "Promotion",color: Color(0xFF4A3298)),
              Menus(),
            ],
          ),
        ),
    );
  }
}


/*

import 'package:flutter/material.dart';
import 'package:rportfarmer/ui/home/widget/banners.dart';
import 'package:rportfarmer/ui/home/widget/menus.dart';
class DashBoard extends StatefulWidget {
  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Banners(),
            Menus(),
          ],
        ),
      ),
    );
  }
}
 
**/