import 'dart:io';
import 'package:flutter/material.dart';
import 'package:rportfarmer/common/constants.dart';
import 'package:rportfarmer/common/default_button.dart';
import 'package:rportfarmer/screens/widgets/input_normal.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rportfarmer/viewmodels/receipt_view_model.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rportfarmer/services/dialog_service.dart';

import '../../locator.dart';

class Receipt extends StatelessWidget {
  final menuID;
  final amountController = TextEditingController();
  final DialogService _dialogService = locator<DialogService>();

  Receipt({Key key, this.menuID}) : super(key: key);

  Widget panelPicture(imageFile) {
    return Card(
      child: Stack(
        children: <Widget>[
          imageFile == null
              ? Padding(
                  padding: const EdgeInsets.all(5),
                  child: Image.asset(
                    'assets/images/imageupload.png',
                    fit: BoxFit.cover,
                    width: 400.00,
                    height: 320.00,
                  ),
                )
              : Image.file(File(imageFile.path))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelProvider<ReceiptViewModel>.withConsumer(
      viewModelBuilder: () => ReceiptViewModel(),
      builder: (context, model, child) => Scaffold(
        appBar: AppBar(
            title: Text(menus[menuID]["text"]),
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              color: Colors.white,
              onPressed: () {
                Navigator.pop(context);
              },
            )),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 30.0),
            color: white,
            child: Column(
              children: [
                SizedBox(height: 15),
                NormalInput(
                  controller: amountController,
                  title: 'จำนวนเงิน',
                  maxline: 1,
                  textInputType: TextInputType.number,
                ),
                Row(
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.photo_camera,
                        size: 30,
                      ),
                      onPressed: () {
                        model.cameraOrGallery(ImageSource.camera);
                      },
                      color: Colors.black,
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.image,
                        size: 30,
                      ),
                      onPressed: () {
                        model.cameraOrGallery(ImageSource.gallery);
                      },
                      color: Colors.red,
                    ),
                  ],
                ),
                panelPicture(model.imageFile),
                SizedBox(
                  height: 5,
                ),
                DefaultButton(
                  text: "บันทึกข้อมูล",
                  press: () {
                    if (amountController.text == '') {
                      _dialogService.showDialog(
                        title: 'ข้อผิดพลาด',
                        description: 'กรุณาป้อนจำนวนเงิน',
                      );
                      return;
                    }
                    if (model.imageFile == null && menuID == 0) {
                      _dialogService.showDialog(
                        title: 'ข้อผิดพลาด',
                        description: 'กรุณาใส่รูปภาพ',
                      );
                      return;
                    }
                    model.saveData(
                      amount: double.parse(amountController.text),
                      menuID: menuID+1,
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
