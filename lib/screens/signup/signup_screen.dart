import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rportfarmer/common/busy_button.dart';
import 'package:rportfarmer/common/constants.dart';
import 'package:rportfarmer/common/input_field.dart';
import 'package:rportfarmer/viewmodels/signup_view_model.dart';

class SignUpScreen extends StatelessWidget {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final fullNameController = TextEditingController();
  final factoryController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ViewModelProvider<SignUpViewModel>.withConsumer(
      viewModelBuilder: () => SignUpViewModel(),
      builder: (context, model, child) => Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Sign Up',
                style: TextStyle(
                  fontSize: 38,
                ),
              ),
              verticalSpaceLarge,
              InputField(
                placeholder: 'ชื่อ-นามสกุล',
                controller: fullNameController,
                maxlenght: 100,
              ),
              verticalSpaceSmall,
              InputField(
                placeholder: 'เบอร์โทรศัพท์',
                controller: emailController,
                textInputType: TextInputType.number,
                maxlenght: 10,
              ),
              InputField(
                placeholder: 'รหัสโรงงาน',
                controller: factoryController,
                textInputType: TextInputType.number,
                maxlenght: 5,
              ),
              verticalSpaceSmall,
              InputField(
                placeholder: 'รหัส PIN',
                password: true,
                controller: passwordController,
                textInputType: TextInputType.number,
                maxlenght: 6,
                additionalNote: 'ตัวเลข 6 ตัว',
              ),
              verticalSpaceSmall,
              verticalSpaceMedium,
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  BusyButton(
                    title: 'Sign Up',
                    busy: model.busy,
                    onPressed: () {
                      model.signUp(
                          email: emailController.text,
                          password: passwordController.text,
                          fullName: fullNameController.text,
                          factory: factoryController.text);
                    },
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
