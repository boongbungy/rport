import 'package:flutter/material.dart';

class NormalInput extends StatefulWidget {
  final TextEditingController controller;
  final String title;
  final int maxline;
  final TextInputType textInputType;

  NormalInput({
    @required this.controller,
    this.title,
    this.maxline,
    this.textInputType,
  });

  @override
  _NormalInputState createState() => _NormalInputState();
}

class _NormalInputState extends State<NormalInput> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            widget.title,
            style: TextStyle(
              fontSize: 15,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          TextFormField(
            controller: widget.controller,
            maxLines: widget.maxline,
            keyboardType: widget.textInputType,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xfff3f3f4), width: 1.0),
                ),
                fillColor: Color(0xfff3f3f4),
                filled: true,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(3.0)),
                )),
          ),
        ],
      ),
    );
  }
}
