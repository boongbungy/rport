import 'package:flutter/material.dart';
import 'package:rportfarmer/size_config.dart';
import 'no_account_text.dart';
import 'sign_form.dart';
//import 'sign_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 20),
                Text(
                  "Welcome Back",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "Sign in with your email and password  \nor continue with social media",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20),//SizeConfig.screenHeight * 0.08
                SignForm(),
                SizedBox(height: 20),//SizeConfig.screenHeight * 0.08

              //  SizedBox(height: 20),
              //  NoAccountText(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
