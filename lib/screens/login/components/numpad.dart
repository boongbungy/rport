import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rportfarmer/common/constants.dart';
import 'package:rportfarmer/common/route_names.dart';
import 'package:rportfarmer/viewmodels/login_view_model.dart';

class NumPad extends StatelessWidget {
  final String numpad;
  final int lenght;
  final bool busy;
  final Function signup;

  NumPad({Key key, this.numpad, this.lenght, this.busy,this.signup}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('กรุณาป้อนรหัส PIN'),
          Preview(text: numpad, length: lenght),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              NumpadButton(
                  text: '1',
                  onPressed: () {
                    Provider.of<LoginViewModel>(context, listen: false)
                        .setNumPadValue('1');
                  }),
              NumpadButton(
                  text: '2',
                  onPressed: () {
                    Provider.of<LoginViewModel>(context, listen: false)
                        .setNumPadValue('2');
                  }),
              NumpadButton(
                  text: '3',
                  onPressed: () {
                    Provider.of<LoginViewModel>(context, listen: false)
                        .setNumPadValue('3');
                  }),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              NumpadButton(
                  text: '4',
                  onPressed: () {
                    Provider.of<LoginViewModel>(context, listen: false)
                        .setNumPadValue('4');
                  }),
              NumpadButton(
                  text: '5',
                  onPressed: () {
                    Provider.of<LoginViewModel>(context, listen: false)
                        .setNumPadValue('5');
                  }),
              NumpadButton(
                  text: '6',
                  onPressed: () {
                    Provider.of<LoginViewModel>(context, listen: false)
                        .setNumPadValue('6');
                  }),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              NumpadButton(
                  text: '7',
                  onPressed: () {
                    Provider.of<LoginViewModel>(context, listen: false)
                        .setNumPadValue('7');
                  }),
              NumpadButton(
                  text: '8',
                  onPressed: () {
                    Provider.of<LoginViewModel>(context, listen: false)
                        .setNumPadValue('8');
                  }),
              NumpadButton(
                  text: '9',
                  onPressed: () {
                    Provider.of<LoginViewModel>(context, listen: false)
                        .setNumPadValue('9');
                  }),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              NumpadButton(haveBorder: false),
              NumpadButton(
                  text: '0',
                  onPressed: () {
                    Provider.of<LoginViewModel>(context, listen: false)
                        .setNumPadValue('0');
                  }),
              NumpadButton(
                  haveBorder: false,
                  icon: Icons.backspace,
                  onPressed: () {
                    Provider.of<LoginViewModel>(context, listen: false)
                        .removeNumPadValue();
                  }),
            ],
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            busy == true
                ? CircularProgressIndicator(
                    strokeWidth: 3,
                    valueColor: AlwaysStoppedAnimation(
                      Theme.of(context).primaryColor,
                    ),
                  )
                : Container(),
            Text(
              "Don’t have an account? ",
              style: TextStyle(fontSize: 16),
            ),GestureDetector(
              onTap: () => signup(),
              child: Text(
                "Sign Up",
                style: TextStyle(
                    fontSize: 16,
                    color: kPrimaryColor),
              ),
            ),
          ])
        ],
      ),
    ));
  }
}

class Preview extends StatelessWidget {
  final int length;
  final String text;

  const Preview({Key key, this.length, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> previewLength = [];
    for (var i = 0; i < length; i++) {
      previewLength.add(Dot(isActive: text.length >= i + 1));
    }
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10.0),
        child: Wrap(children: previewLength));
  }
}

class Dot extends StatelessWidget {
  final bool isActive;

  const Dot({Key key, this.isActive = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: Container(
        width: 15.0,
        height: 15.0,
        decoration: BoxDecoration(
          color: isActive ? Theme.of(context).primaryColor : Colors.transparent,
          border: Border.all(width: 1.0, color: Theme.of(context).primaryColor),
          borderRadius: BorderRadius.circular(15.0),
        ),
      ),
    );
  }
}

class NumpadButton extends StatelessWidget {
  final String text;
  final IconData icon;
  final bool haveBorder;
  final Function onPressed;

  const NumpadButton(
      {Key key, this.text, this.icon, this.haveBorder = true, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextStyle buttonStyle =
        TextStyle(fontSize: 22.0, color: Theme.of(context).primaryColor);
    Widget label = icon != null
        ? Icon(
            icon,
            color: Theme.of(context).primaryColor.withOpacity(0.8),
            size: 35.0,
          )
        : Text(this.text ?? '', style: buttonStyle);

    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: OutlineButton(
        borderSide:
            haveBorder ? BorderSide(color: Colors.grey[400]) : BorderSide.none,
        highlightedBorderColor: icon != null
            ? Colors.transparent
            : Theme.of(context).primaryColor.withOpacity(0.3),
        splashColor: icon != null
            ? Colors.transparent
            : Theme.of(context).primaryColor.withOpacity(0.1),
        padding: EdgeInsets.all(20.0),
        shape: CircleBorder(),
        onPressed: onPressed,
        child: label,
      ),
    );
  }
}
