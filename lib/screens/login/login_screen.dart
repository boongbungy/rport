
import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:rportfarmer/screens/login/components/numpad.dart';
import 'package:rportfarmer/viewmodels/login_view_model.dart';


class LoginScreen extends StatelessWidget {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    return ViewModelProvider<LoginViewModel>.withConsumer(
      viewModelBuilder: () => LoginViewModel(),
      builder: (context, model, child) => Scaffold(
          backgroundColor: Colors.white,
          body: Container(
              child:  NumPad(numpad:model.currentNumPad,lenght:model.initLenght,busy:model.busy,signup:model.navigateToSignUp),
          ),
      ),
    );
  }
}
