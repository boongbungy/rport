import 'package:flutter/material.dart';
class Panel extends StatelessWidget {

  Widget Btn(String title){
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 30),
      margin: EdgeInsets.only(right: 16),
      decoration: BoxDecoration(
          color: Color(0xff29404E),
          borderRadius: BorderRadius.circular(12)
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          //Image.asset(imgAssetPath, height: 27,),
          SizedBox(height: 12,),
          Text(title, style: TextStyle(
              color: Colors.white
          ),)
        ],
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      child: Row(
        children: <Widget>[
          Btn('ใบเสร็จ'),
          Btn('ค่าใส่ปุ๋ย'),

          FlatButton(

            onPressed: () {
              /*...*/
            },
            color: Colors.orange,
            child: Column(
              children: <Widget>[
                Icon(Icons.add),
                Text('ADD')
              ],
            ),
          ),
        ],
      ),
    );
  }
}
