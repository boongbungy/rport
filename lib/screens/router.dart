import 'package:flutter/material.dart';
import 'package:rportfarmer/common/route_names.dart';
import 'package:rportfarmer/screens/home/home_screen.dart';
import 'package:rportfarmer/screens/login/login_screen.dart';
import 'package:rportfarmer/screens/signup/signup_screen.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case LoginScreenRoute:
      return MaterialPageRoute(builder: (context) => LoginScreen());
    /*_getPageRoute(
        routeName: settings.name,
        viewToShow: LoginScreen(),
      );*/
    case HomeScreenRoute:
      return MaterialPageRoute(builder: (context) => HomeScreen());
    /*return _getPageRoute(
        routeName: settings.name,
        viewToShow: HomeScreen(),
      );*/
    case SignUpViewRoute:
      return MaterialPageRoute(builder: (context) => SignUpScreen());
    default:
      return MaterialPageRoute(
          builder: (_) => Scaffold(
                body: Center(
                    child: Text('No route defined for ${settings.name}')),
              ));
  }
}

PageRoute _getPageRoute({String routeName, Widget viewToShow}) {
  return MaterialPageRoute(
      settings: RouteSettings(
        name: routeName,
      ),
      builder: (BuildContext context) => viewToShow);
}
