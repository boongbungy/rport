import 'package:get_it/get_it.dart';
import 'package:rportfarmer/services/authentication_service.dart';
import 'package:rportfarmer/services/dialog_service.dart';
import 'package:rportfarmer/services/dio_service.dart';
import 'package:rportfarmer/services/navigation_service.dart';
import 'package:rportfarmer/services/storage_service.dart';
GetIt locator = GetIt.instance;
void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => DialogService());
  locator.registerLazySingleton(() => AuthenticationService());
  locator.registerLazySingleton(() => DioService());
  locator.registerLazySingleton(() => StorageService());
}