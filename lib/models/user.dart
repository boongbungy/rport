class User {
  final String id;
  final String fullName;
  final String telephone;
  final String token;
  final String factory;

  User({this.id, this.fullName, this.telephone, this.token, this.factory});

  User.fromData(Map<String, dynamic> data)
      : id = data['id'],
        fullName = data['fullName'],
        telephone = data['telephone'],
        token = data['token'],
        factory = data['factory'];

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'fullName': fullName,
      'telephone': telephone,
      'token': token,
      'factory': factory,
    };
  }
}
