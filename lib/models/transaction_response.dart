// To parse this JSON data, do
//
//     final productResponse = productResponseFromJson(jsonString);

import 'dart:convert';

import 'dart:ffi';

List<TransactionResponse> transactionResponseFromJson(String str) => List<TransactionResponse>.from(json.decode(str).map((x) => TransactionResponse.fromJson(x)));

String transactionResponseToJson(List<TransactionResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class TransactionResponse {
  TransactionResponse({
    this.id,
    this.user,
    this.amount,
    this.image,
    this.typereceipt,
    this.factory,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int user;
  int factory;
  String image;
  int typereceipt;
  double amount;
  DateTime createdAt;
  DateTime updatedAt;

  factory TransactionResponse.fromJson(Map<String, dynamic> json) => TransactionResponse(
    id: json["id"],
    user: json["user"],
    factory: json["factory"],
    image: json["image"],
    typereceipt: json["typereceipt"],
    amount: double.parse(json["amount"]),
    //createdAt: DateTime.parse(json["createdAt"]),
    //updatedAt: DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user": user,
    "factory": factory,
    "image": image,
    "typereceipt": typereceipt,
    "amount": amount,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
  };
}