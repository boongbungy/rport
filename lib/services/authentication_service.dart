

import 'package:flutter/foundation.dart';
import 'package:rportfarmer/models/user.dart';
import 'package:rportfarmer/services/dio_service.dart';

import '../locator.dart';
import 'navigation_service.dart';

class AuthenticationService {
  //final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  //final FirestoreService _firestoreService = locator<FirestoreService>();
  final DioService _dioService = locator<DioService>();
  final NavigationService _navigationService = locator<NavigationService>();

  User _currentUser;
  User get currentUser => _currentUser;
  Future loginWithPin({
    @required String username,
    @required String password,
  }) async {
    var result = await _dioService.AuthDjango(username:username,password:password);

    return result;

    /*try {
      var authResult = await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      await _populateCurrentUser(authResult.user);
      return authResult.user != null;
    } catch (e) {
      return e.message;
    }*/
  }
  /*
  Future loginWithEmail({
    @required String email,
    @required String password,
  }) async {
    try {
      var authResult = await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      await _populateCurrentUser(authResult.user);
      return authResult.user != null;
    } catch (e) {
      return e.message;
    }
  }*/

  /*Future signUpWithEmail({
    @required String email,
    @required String password,
    @required String fullName,
    @required String role,
  }) async {
    try {
      var authResult = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      // create a new user profile on firestore
      _currentUser = User(
        id: authResult.user.uid,
        email: email,
        fullName: fullName,
        userRole: role,
      );

      await _firestoreService.createUser(_currentUser);

      return authResult.user != null;
    } catch (e) {
      return e.message;
    }
  }*/

  Future<bool> isUserLoggedIn() async {
    //var user = await _firebaseAuth.currentUser();

    //await _populateCurrentUser(user);
    //return user != null;
    return null;
  }
  /*
  Future _populateCurrentUser(FirebaseUser user) async {
    // await _firebaseAuth.signOut();
    if (user != null) {
      _currentUser = await _firestoreService.getUser(user.uid);
    }

  }*/
  /*
  Future<void> signOut() async {
    await _firebaseAuth.signOut().then((response) {
      _navigationService.navigateTo(LoginViewRoute);
      /*MaterialPageRoute materialPageRoute =
          MaterialPageRoute(builder: (BuildContext context) => Home());
      Navigator.of(context).pushAndRemoveUntil(
          materialPageRoute, (Route<dynamic> route) => false);*/
    });
  }*/
}
