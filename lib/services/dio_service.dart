import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import 'package:http_parser/http_parser.dart';
import 'package:rportfarmer/models/transaction_response.dart';

import 'headerInterceptor.dart';
import 'package:intl/intl.dart';

class DioService {
  //singleton
  static final _instant = DioService._interal();

  DioService._interal();

  factory DioService() => _instant;

  //static final _dio = Dio()..interceptors.add(HeaderInterceptor());
  static final _dio = Dio()..interceptors;
  static final _dioAuthen = Dio()..interceptors;

  //static const BASE_URL = 'https://srisudasystem.com';

  static const BASE_URL = 'http://192.168.1.106:8002'; //
  get imagePath => "$BASE_URL/images/";

  Future AuthDjango({String username, String password}) async {
    Response response = await _dioAuthen.post('$BASE_URL/api/token/',
        data: {'username': username, 'password': password});

    if (response.statusCode == 200) {
      return response.data;
    }

    return null;
  }

  Future SignUp(
      {@required String email,
      @required String password,
      @required String fullName,
      @required String factory}) async {
    try {
      Response response = await _dio.post('$BASE_URL/api/register', data: {
        'username': email,
        'password': password,
        'first_name': fullName,
        'factory': factory,
      });
      if (response.statusCode == 200) {
        print(response);
      } else {
        return null;
      }
    } on DioError catch (ex) {
      print(ex.error.toString());

      if (ex.type == DioErrorType.RESPONSE) {
        //404, 400, 500
        if (ex.response.statusCode == 400) {
          //give warning here
          print(ex.response);
        }
      } else if (ex.type == DioErrorType.DEFAULT) {
        throw Exception(ex.error.message);
      } else {
        //timeout and canceled
      }
    }
  }

  Future ReceiptSum() async {
    Response response = await _dio.get('$BASE_URL/api/ReceiptSum');
    print(response.data);
    if (response.statusCode == 200) {
      //final List<ProductResponse> result = productResponseFromJson(jsonEncode(response.data));
      return response.data['amount'];
    }
    throw Exception("NETWORK FAIL");
  }

  Future<List<TransactionResponse>> getData() async {
    Response response = await _dio.get('$BASE_URL/api/Receipt');
    if (response.statusCode == 200) {
      //final List<ProductResponse> result = productResponseFromJson(jsonEncode(response.data));
      return transactionResponseFromJson(jsonEncode(response.data));
    }
    throw Exception("NETWORK FAIL");
  }

  //Future<String> addReceipt()
  Future addReceipt({TransactionResponse transaction, File file}) async {
    FormData data = FormData.fromMap({
      "factory": transaction.factory,
      "amount": transaction.amount,
      "typereceipt": transaction.typereceipt,
      "user": transaction.user,
      "datein": DateFormat('yyyy-MM-dd').format(DateTime.now()),
      if (file != null)
        "image": await MultipartFile.fromFile(
          file.path,
          contentType: MediaType("image", "jpg"),
        ),
    });

    Response response = await _dio.post('$BASE_URL/api/Receipt', data: data);
    print(response.statusCode);
    if (response.statusCode == 201) {
      return 201;
    }

    throw Exception("NETWORK FAIL");
  }
}
