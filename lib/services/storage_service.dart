
import 'package:localstorage/localstorage.dart';

class StorageService {
  final LocalStorage _storage = new LocalStorage('user');
  void savePost(String key,String value) async {
    await _storage.ready;
    _storage.setItem(key, value);
  }
  Future<String> getLocalStorage(String key) async{
    await _storage.ready;
    String data = _storage.getItem(key);
    if(data==null){
      return null;
    }

    return data;
  }


  //Future&lt;PostItem&gt; getPostFromCache() async {<br>    await storage.ready;<br>    Map&lt;String, dynamic&gt; data = storage.getItem('post');<br>    if (data == null) {<br>      return null;<br>    }<br>    PostItem post = PostItem.fromJson(data);<br>    post.fromCache = true; //to indicate post is pulled from cache<br>    return post;<br>  }</pre>
}