import 'package:dio/dio.dart';
import 'package:rportfarmer/locator.dart';
import 'package:rportfarmer/services/storage_service.dart';

class HeaderInterceptor extends Interceptor {
  final StorageService _storage = locator<StorageService>();

  @override
  Future onRequest(RequestOptions options) async {
    var access = await _storage.getLocalStorage('access');
    final headers = {
      "Authorization": "Bearer " +access,
    };
    //options.headers["Authorization"] = "Bearer " + token;
    options.headers.addAll(headers);
    return super.onRequest(options);
  }

  @override
  Future onResponse(Response response) {
    // TODO: implement onResponse
    return super.onResponse(response);
  }
}
