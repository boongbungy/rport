import 'package:flutter/material.dart';

class NavigationService {
  GlobalKey<NavigatorState> navigationKey = GlobalKey<NavigatorState>();

  //GlobalKey<NavigatorState> get navigationKey => _navigationKey;

  void pop() {
    return navigationKey.currentState.pop();
  }

  Future<dynamic> navigateTo(String routeName, {dynamic arguments}) {
    print(routeName);
    return navigationKey.currentState.pushNamed(routeName, arguments: arguments);
  }
}
