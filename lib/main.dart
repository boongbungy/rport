import 'package:flutter/material.dart';
import 'package:rportfarmer/screens/StartUpView.dart';
import 'package:rportfarmer/screens/login/login_screen.dart';
import 'package:rportfarmer/screens/router.dart' as router;
import 'package:rportfarmer/common/route_names.dart' as routes;
import 'package:rportfarmer/services/dialog_service.dart';
import 'package:rportfarmer/services/navigation_service.dart';
import 'locator.dart';
import 'managers/dialog_manager.dart';


void main() {
  // Register all the models and services before the app starts
  setupLocator();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rport',
      debugShowCheckedModeBanner: false,
      builder: (context, child) => Navigator(
        key: locator<DialogService>().dialogNavigationKey,
        onGenerateRoute: (settings) => MaterialPageRoute(
            builder: (context) => DialogManager(child: child)),
      ),
      navigatorKey: locator<NavigationService>().navigationKey,
      theme: ThemeData(
        primaryColor: Color(0xff19c7c1),
        textTheme: Theme.of(context).textTheme.apply(
          fontFamily: 'Open Sans',
        ),
      ),
      home: StartUpView(),
      onGenerateRoute: router.generateRoute,
      initialRoute: routes.LoginScreenRoute,
    );
  }
}


