
//import 'package:rportfarmer/models/transaction_response.dart';
import 'package:rportfarmer/services/dio_service.dart';
//import 'package:rportfarmer/services/storage_service.dart';

import '../locator.dart';
import 'base_model.dart';

class HomeViewModel extends BaseModel {

  double _amount = 0;
  double get amount => _amount;

  final DioService _dioService = locator<DioService>();
  //final StorageService _storage = locator<StorageService>();

  Future handleData() async {


   // var access = await _storage.getLocalStorage('access');
   // print('access');
   // print(access);
    setBusy(true);
    var r = await _dioService.ReceiptSum();
    _amount = r;
    print(r);
    notifyListeners();
    setBusy(false);
  }
}