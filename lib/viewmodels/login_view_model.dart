
import 'package:flutter/foundation.dart';
import 'package:rportfarmer/common/route_names.dart';
import 'package:rportfarmer/services/authentication_service.dart';
import 'package:rportfarmer/services/dialog_service.dart';
import 'package:rportfarmer/services/navigation_service.dart';
import 'package:rportfarmer/services/storage_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:localstorage/localstorage.dart';
import '../locator.dart';
import 'base_model.dart';

class LoginViewModel extends BaseModel {
  final AuthenticationService _authenticationService = locator<AuthenticationService>();
  final DialogService _dialogService = locator<DialogService>();
  final NavigationService _navigationService = locator<NavigationService>();
  //final LocalStorage storage = new LocalStorage('user');

  final StorageService _storage = locator<StorageService>();


  int initLenght = 4;
  String numPad = '';
  String get currentNumPad => numPad;

  void setNumPadValue(String val) {
    print(val);
    numPad += val;
    if(numPad.length == initLenght) {
      login(username:'0984564591',password:numPad);
    }
    notifyListeners();
  }
  void removeNumPadValue() {
    if (numPad != '') {
      numPad = numPad.substring(0, numPad.length - 1);
    }
    notifyListeners();
  }
  Future login({
    @required String username,
    @required String password,
  }) async {
    setBusy(true);

    var result = await _authenticationService.loginWithPin(
      username: username,
      password: password,
    );

    //print(result['factory'][0]['factory']);
    /*if(result==null){
      await _dialogService.showDialog(
        title: 'Login Failure',
        description: 'General login failure. Please try again later',
      );
    }
    _storage.savePost('access', result['access']);
    _storage.savePost('refresh', result['refresh']);
    _storage.savePost('user', result['userid'].toString());
    _storage.savePost('factory', '1');
    setBusy(false);*/
    _navigationService.navigateTo(HomeScreenRoute);
    /*if (result is bool) {
      if (result) {

        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('user', "value");
        _navigationService.navigateTo(HomeScreenRoute);
      } else {
        await _dialogService.showDialog(
          title: 'Login Failure',
          description: 'General login failure. Please try again later',
        );
      }
    } else {
      await _dialogService.showDialog(
        title: 'Login Failure',
        description: result,
      );
    }*/

  }

  void navigateToSignUp() {
    _navigationService.navigateTo(SignUpViewRoute);
  }
}
