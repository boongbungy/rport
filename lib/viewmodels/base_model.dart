import 'package:flutter/widgets.dart';
import 'package:rportfarmer/models/user.dart';
import 'package:rportfarmer/services/authentication_service.dart';


import '../locator.dart';

class BaseModel extends ChangeNotifier {
  final AuthenticationService _authenticationService =
  locator<AuthenticationService>();

  User get currentUser => _authenticationService.currentUser;

  bool _busy = false;
  int _typeacid = 1;
  DateTime _datepicker;
  bool get busy => _busy;
  int get typeacid => _typeacid;
  DateTime get datepicker => _datepicker;
  void setBusy(bool value) {
    _busy = value;
    notifyListeners();
  }

  void setTypeAcid(int value) {
    _typeacid = value;
    notifyListeners();
  }

  void setDatepicker(DateTime d) {
    _datepicker = d;
    notifyListeners();
  }
}
