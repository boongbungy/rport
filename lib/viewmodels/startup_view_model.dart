



import 'package:rportfarmer/common/route_names.dart';
import 'package:rportfarmer/services/authentication_service.dart';
import 'package:rportfarmer/services/navigation_service.dart';
import 'package:rportfarmer/common/route_names.dart' as routes;
import '../locator.dart';
import 'base_model.dart';


class StartUpViewModel extends BaseModel {

  final AuthenticationService _authenticationService =
  locator<AuthenticationService>();
  final NavigationService _navigationService = locator<NavigationService>();

  Future handleStartUpLogic() async {
    _navigationService.navigateTo(routes.LoginScreenRoute);
    /*print('XXXX');
    var hasLoggedInUser = await _authenticationService.isUserLoggedIn();
    print('hasLoggedInUser is $hasLoggedInUser');
    if (hasLoggedInUser) {
      print('go home');
     // final arguments = PassPageArguments('Passed One', 'Passed Two');
      _navigationService.navigateTo(HomeScreenRoute);//,arguments: arguments
    } else {
      print('go login');
      _navigationService.navigateTo(LoginScreenRoute);
    }*/
  }

}
