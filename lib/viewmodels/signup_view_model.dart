
import 'package:flutter/foundation.dart';
import 'package:rportfarmer/services/dialog_service.dart';
import 'package:rportfarmer/services/dio_service.dart';
import 'package:rportfarmer/services/navigation_service.dart';

import 'base_model.dart';
import '../locator.dart';
class SignUpViewModel extends BaseModel {

  final DialogService _dialogService = locator<DialogService>();
  final NavigationService _navigationService = locator<NavigationService>();
  final DioService _dioService = locator<DioService>();
  Future signUp({
    @required String email,
    @required String password,
    @required String fullName,
    @required String factory,
  }) async {
      setBusy(true);
      if(email == ''){
        await _dialogService.showDialog(
          title: 'ข้อผิดพลาด',
          description: 'กรุณาป้อน เบอร์โทรศัพท์',
        );
        setBusy(false);
        return;
      }
      if(password == '' || password.length < 6){
        await _dialogService.showDialog(
          title: 'ข้อผิดพลาด',
          description: 'กรุณาป้อน ชื่อ รหัส PIN 6 หลัก',
        );
        setBusy(false);
        return;
      }

      if(factory == '' || factory.length < 5){
        await _dialogService.showDialog(
          title: 'ข้อผิดพลาด',
          description: 'กรุณาป้อน ชื่อ รหัสโรงงาน',
        );
        setBusy(false);
        return;
      }
      if(fullName == ''){
        await _dialogService.showDialog(
          title: 'ข้อผิดพลาด',
          description: 'กรุณาป้อน ชื่อ สกุล',
        );
        setBusy(false);
        return;
      }
      var result = await _dioService.SignUp(
        email: email,
        password: password,
        fullName: fullName,
        factory: factory,
      );

  }
}