import 'dart:io';



import 'package:image_picker/image_picker.dart';
import 'package:rportfarmer/models/transaction_response.dart';
import 'package:rportfarmer/common/route_names.dart' as routes;
import 'package:rportfarmer/services/dialog_service.dart';
import 'package:rportfarmer/services/dio_service.dart';

import 'package:rportfarmer/services/navigation_service.dart';

import '../locator.dart';
import 'base_model.dart';

class ReceiptViewModel extends BaseModel {
  final NavigationService _navigationService = locator<NavigationService>();
  final DioService _dioService = locator<DioService>();
  final DialogService _dialogService = locator<DialogService>();
  final ImagePicker _picker = ImagePicker();
  PickedFile _imageFile;

  PickedFile get imageFile => _imageFile;

  Future saveData({double amount,int menuID}) async {
    setBusy(true);
    //List<TransactionResponse> r = await _dioService.getData();
    print('menuID $menuID');

    var result = await _dioService.addReceipt(
        transaction: TransactionResponse(
          amount: amount,
          factory: 1,
          typereceipt: menuID,
          user: 1,
        ),
        file: _imageFile != null ? File(_imageFile.path) : null
    );
    setBusy(false);
    if (result == 201) {
      await _dialogService.showDialog(
        title: 'บันทึกข้อมูล',
        description: 'บันทึกข้อมูลสำเร็จแล้ว',
      );
      _navigationService.navigateTo(routes.HomeScreenRoute);
      //_navigationService.pop();
    } else {
      await _dialogService.showDialog(
        title: 'บันทึกข้อมูลไม่สำเร็จ',
        description: result,
      );
    }
  }

  Future<void> cameraOrGallery(ImageSource imageSource) async {
    try {
      final pickedFile = await _picker.getImage(
        source: imageSource,
        maxHeight: 200.0,
        maxWidth: 400.0,
      );
      _imageFile = pickedFile;
      notifyListeners();
    } catch (e) {
      print('error $e');
    }
  }
}
